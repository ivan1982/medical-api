# Getting Started with Simple Medical Plan API
This project was built with react + stripe API.
Public API host 
### http://47.243.158.165:1337
Feature GET: /features\
Plan GET: /plans


## How to setup Node API
Git clone the source code\
### run docker-compose pull
### run docler-compose up
Strapi admin panel will be started up, set up the user, \
and login create Feature and Plan content type
